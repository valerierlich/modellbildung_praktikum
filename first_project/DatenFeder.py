import matplotlib.pyplot as plt
import csv
from pylab import *

# Plot velocity
x = []
y = []

with open('DatenFeder.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for row in plots:
        x.append(float(row[0]))
        y.append(float(row[1]))

(m,b) = polyfit(x,y,1)
print(b)
print(m)

yp = polyval([m,b],x)
plot (x,yp)
scatter(x,y)
grid(True)
xlabel('x')
ylabel('y')

show()
