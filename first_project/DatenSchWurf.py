import matplotlib.pyplot as plt
import csv
from pylab import *

x = []
y = []
y2 = []
y3 = []
y4 = []

with open('DatenSchWurf.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for row in plots:
        x.append(float(row[0]))
        y.append(float(row[1]))
        y2.append(float(row[2]))
        y3.append(float(row[3]))
        y4.append(float(row[4]))

(m,b) = polyfit(x,y,1)
(m2,b2) = polyfit(x,y2,1)
(m3,b3) = polyfit(x,y3,1)
(m4,b4) = polyfit(x,y4,1)

yp = polyval([m,b],x)
yp2 = polyval([m2,b2],x)
yp3 = polyval([m3,b3],x)
yp4 = polyval([m4,b3],x)

plot (x,yp)
plot (x,yp2)
plot (x,yp3)
plot (x,yp4)

scatter(x,y)
scatter(x,y2)
scatter(x,y3)
scatter(x,y4)

grid(True)
xlabel('x')
ylabel('y')

show()
