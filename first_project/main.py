import matplotlib.pyplot as plt
import csv
import decimal

# Plot velocity
x = []
y = []

# Plot height
x1 = []
y1 = []

# Plot acceleration
x2 = []
y2 = []

with open('PressureAndVelocity.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for row in plots:
        x.append(float(decimal.Decimal(row[0])))
        y.append(float(decimal.Decimal(row[4])))

        x1.append(float(decimal.Decimal(row[0])))
        y1.append(float(decimal.Decimal(row[2])))

with open('Acceleration.csv', 'r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for row in plots:
        x2.append(float(decimal.Decimal(row[0])))
        y2.append(float(decimal.Decimal(row[1])))

plt.plot(x, y, label='Velocity (m/s)')
plt.xlabel('x')
plt.ylabel('y')

plt.plot(x1, y1, label='Height(m)')
plt.xlabel('x')
plt.ylabel('y')

plt.plot(x2, y2, label = 'Acceleration')
plt.xlabel('x')
plt.ylabel('y')

plt.title('Aufgabe 1')
plt.legend()
plt.show()
