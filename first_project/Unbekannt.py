import matplotlib.pyplot as plt
import numpy as np
from pylab import *
import tensorflow as tf

x = []
y = []
z = []

file = open('UnbekannteDaten.txt','r')
for line in file:
    fields = line.split(' ')
    x.append(float(fields[0]))
    y.append(float(fields[1]))

fit = np.polyfit(x,y,3)
fit_fn = np.poly1d(fit) 
# fit_fn is now a function which takes in x and returns an estimate for y

tensor = tf.convert_to_tensor(x,tf, float)

print(tensor)
 
plt.show()